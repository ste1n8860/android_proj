package com.example.app3_5;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class INSERTActivity extends AppCompatActivity {

    EditText etInsert;
    String dateTime, text;
    Boolean checkEmptyEDIT;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab_save);
       /* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
               // insertData();
            }
        });*/
        fab.setOnTouchListener(new View.OnTouchListener() {

            float startX;
            float startRawX;
            float distanceX;
            int lastAction;

            @Override
            public boolean onTouch(View v, MotionEvent event){
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = v.getX() - event.getRawX();
                        startRawX = event.getRawX();
                        lastAction = MotionEvent.ACTION_DOWN;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        v.setX(event.getRawX() + startX);

                        lastAction = MotionEvent.ACTION_MOVE;
                        break;

                    case MotionEvent.ACTION_UP:
                        distanceX = event.getRawX() - startRawX;
                        if (Math.abs(distanceX) <= 10) {
                            insertData();
                        }
                        break;
                    case MotionEvent.ACTION_BUTTON_PRESS:

                    default:
                        return false;
                }
                return true;
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etInsert = (EditText) findViewById(R.id.et_insert_activity);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    protected void onResume(){
        //Boolean mes = sp.getBoolean("notif", false);
        //float address = sp.getFloat("address",15.0f );
       // String address = sp.getString("address", "");
       // String info = "Сообщаю, что... " + ((mes)?  "записан Адрес  " + address: " НЕТ ");
       // etInsert.setText(info);
       // float fonts = sp.getFloat("font_list", 13.0f);
       // if (sp.getFloat("font_list", 13.0f) = true ){
        //    etInsert.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
       // }

       /* String listValue = sp.getString("font_list"," 18 ");
       // float listValueFloat = sp.getFloat("font_list", 13f);
        float abc = Float.parseFloat(listValue);
        etInsert.setTextSize(abc);
        etInsert.setText(" font size " + abc);*/

       //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //размер шрифта через Преференс и ЛистПреференс
        etInsert.setTextSize(Float.parseFloat(sp.getString("font_list"," 18 ")));

        super.onResume();
    }
    @Override
    public void onPause(){
        //insertData();
        super.onPause();
    }

    public void getDateTime(){
        // Текущие дата и время
        //db1 = helper.onCreate();
        Date currentDate = new Date();
        // Форматирование времени как "день.месяц.год"
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String dateText = dateFormat.format(currentDate);
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
        String timeText = timeFormat.format(currentDate);
        String date_time = dateText+ "  " +timeText;

        dateTime = date_time;
    }
    public void insertData(){

        getDateTime();
        text = etInsert.getText().toString();
        //
        checkETemptyOrNot(text, dateTime);

        if (checkEmptyEDIT == true){
            // sqliteQuery = " INSERT INTO listok4 (text, date) VALUES ('" + text + "', '" + dateTime + "' )";
            // db1.execSQL(sqliteQuery);
            SQLiteHelper helper = new SQLiteHelper(getApplicationContext());
            helper.insertData(text, dateTime);

            //спрячем клавиатуру
            //InputMethodManager inputMM = (InputMethodManager) getActivity(getActivity().INPUT_METHOD_SERVICE);
            // inputMM.hideSoftInputFromWindow(etInsert.getWindowToken(), 0);

           // startActivity(new Intent(INSERTActivity.this, MainActivity.class));
            finish();

            // Toast.makeText(this,"Информация внесена УСПЕШНО!!!", Toast.LENGTH_LONG).show();
            // clearETafter();
        }else{
            Toast.makeText(this,"Заполни ВСЕ поля", Toast.LENGTH_LONG).show();
        }
    }


    // public void DBCreate(){
    // db1 = SQLiteDatabase.openOrCreateDatabase("spisok4", null, null); //вообще ХЗ, что это такое
    // db1.execSQL(" CREATE TABLE IF NOT EXISTS listok4 ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , text TEXT , date NUMERIC );");
    //}

    // проверка на заполнение поля с текстом
    public void checkETemptyOrNot(String name, String dateTime){
        // if(TextUtils.isEmpty(name) || TextUtils.isEmpty(dateTime) || TextUtils.isEmpty(subject)){
        if(TextUtils.isEmpty(name)){
            checkEmptyEDIT = false;
        }else {
            checkEmptyEDIT = true;
        }
    }

   // @Override
   // public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
      //  getMenuInflater().inflate(R.menu.main, menu);
     //   startActivity(new Intent(INSERTActivity.this, PREFActivity.class));
      //  return true;
    //}
   public boolean onCreateOptionsMenu(Menu menu) {
       MenuItem mi = menu.add(0, 1, 0, "Настройки");
       mi.setIntent(new Intent(this, PREFActivity.class));
       return super.onCreateOptionsMenu(menu);
   }

   @Override
    public void onBackPressed(){
        insertData();
        finish();
        super.onBackPressed();
   }

}
