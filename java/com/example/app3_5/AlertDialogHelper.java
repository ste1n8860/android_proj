package com.example.app3_5;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

public class AlertDialogHelper extends DialogFragment {

    SharedPreferences sp;
    EditText etInputPass;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        // штука для отображения каких-то вьюшек
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // создаём свой билдер с кастомной вьюшкой
        builder.setView(inflater.inflate(R.layout.dialog_enter_password, null))
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("проверить пароль", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       // sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                       // String passPrefs = sp.getString("et_pass_prefs", "pass2Def");
                       // etInputPass = (EditText) getView().findViewById(R.id.et_dialog_password);
                        //String passInput = etInputPass.getText().toString();
                       // if (passPrefs != passInput){
                       //     Toast.makeText(getActivity(),"Не верный пароль - " + passInput, Toast.LENGTH_LONG).show();
                       // }
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });


        return builder.setTitle("Безопасность!").setMessage("Проверка пароля для расшифровывания ссобщения").create();
    }

   /* @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_enter_password, null))
                // Add action buttons
                .setPositiveButton("DeCrypt", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                    }
                })
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //LoginDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();

    }*/
}
