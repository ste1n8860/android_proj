package com.example.app3_5;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.Inflater;

public class UPDATEActivity extends AppCompatActivity {

    SQLiteHelper helper;

    EditText etUpdate;
    Button  btnCrypt, btnRestore;
    String id, pathToDir;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //-------------------------------------------------
        FloatingActionButton fab = findViewById(R.id.fab_edit);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //etUpdate.setEnabled(true);
               // Snackbar.make(view, "ЗАПИСАНО!", Snackbar.LENGTH_LONG)
                //        .setAction("успешно!", null).show();
                updateData();
            }
        });*/
        fab.setOnTouchListener(new View.OnTouchListener() {

            float startX;
            float startRawX;
            float distanceX;
            int lastAction;

            @Override
            public boolean onTouch(View v, MotionEvent event){
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = v.getX() - event.getRawX();
                        startRawX = event.getRawX();
                        lastAction = MotionEvent.ACTION_DOWN;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        v.setX(event.getRawX() + startX);

                        lastAction = MotionEvent.ACTION_MOVE;
                        break;

                    case MotionEvent.ACTION_UP:
                        distanceX = event.getRawX() - startRawX;
                        if (Math.abs(distanceX) <= 10) {
                            updateData();
                        }
                        break;
                    case MotionEvent.ACTION_BUTTON_PRESS:

                    default:
                        return false;
                }
                return true;
            }
        });

        //-------------------------------------------------------------

        etUpdate = (EditText) findViewById(R.id.et_overwright);
       // btnOver = (Button) findViewById(R.id.btn_overwright_update);
        btnCrypt = (Button) findViewById(R.id.btn_crypt_update);
        btnRestore = (Button) findViewById(R.id.btn_save_to_file_update);

        sp = PreferenceManager.getDefaultSharedPreferences(this);

        //размер шрифта через Преференс и ЛистПреференс
        etUpdate.setTextSize(Float.parseFloat(sp.getString("font_list"," 18 ")));

        final String ids = getIntent().getStringExtra("ids");
        id = ids;
        showDataTV();

        // пути копирования файлов ИЗ и КУДА
        //File sourcePath = new File("/storage/emulated/0/MyNote/file_name.txt");
       // File destinPath = new File("/storage/emulated/0/MyNote/NewPath/file_name.txt");
        //pathToDir = sp.getString("dir_path", "/MyNote/Default/");

        btnRestore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getListFiles();
            }
        });
    }

    public void updateData(){
        String text = etUpdate.getText().toString();
        if (id != null) {
            helper = new SQLiteHelper(getApplicationContext());
            helper.updateRow(id,text,getDateTime());
            //всплывающее сообщение Тост - сбоку слева
            Toast toastDel = Toast.makeText(getApplicationContext(), "записано!!! ", Toast.LENGTH_SHORT);
            toastDel.setGravity(Gravity.LEFT, 150, 50);
            toastDel.show();
            // переход на Активити с Листом
            //startActivity(new Intent(UPDATEActivity.this, MainActivity.class));
            finish();
        } else {
            //всплывающее сообщение Тост - сбоку в центре
            Toast toastDel = Toast.makeText(getApplicationContext()," НЕЧЕГО редактировать!!! ", Toast.LENGTH_SHORT);
            toastDel.setGravity(Gravity.CENTER,250,50);
            toastDel.show();
        }
    }

    //работающий вызов одной строки
    private void showDataTV(){
        //ну как всегда - подключение к базе
        helper = new SQLiteHelper(getApplicationContext());
        //переменная для тестирования получения строки по ИД
        String info = id;
        //условия для ошибки
        if (info.equals(String.valueOf(""))){
            return;
        }
        Cursor cursor = helper.getRow2(id); // курсор передаёт ид в обработку и хватает значение курсора
        String data = null;
        if (cursor.moveToFirst()) {
            do {
                //передаём значение из курсора в строку
                data = cursor.getString(cursor.getColumnIndex("text")) ;
                // "text " + cursor.getString(cursor.getColumnIndexOrThrow("text")) + "\n\n" ;
            } while (cursor.moveToNext());
        }
        cursor.close();
        etUpdate.setText(data);
    }

    // получение даты и времени
    public String getDateTime(){
        // Текущие дата и время
        Date currentDate = new Date();
        // Форматирование времени как "день.месяц.год"
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String dateText = dateFormat.format(currentDate);
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
        String timeText = timeFormat.format(currentDate);
        String date_time = dateText+ "  " +timeText;
        return date_time;
    }

    /*
    @Override
    protected void onPause(){
        super.onPause();
        //insertDataToSQL();
        instance = etUpdate.getText().toString(); // сохранение данных из ТВ
    }
    @Override
    protected void onResume(){
        super.onResume();
        etUpdate.setText(instance); //возвращение данных в ТВ
    } */
    @Override
    protected void onStop(){
        super.onStop();
        finish();
    }
    @Override
    public void onBackPressed(){
        updateData();
        //finish();
        super.onBackPressed();
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuItem mi = menu.add(0, 1, 0, "Настройки");
        //mi.setIntent(new Intent(this, PREFActivity.class));
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.menu_update, menu);
        return super.onCreateOptionsMenu(menu);
    }
    // КОНТЕКСТНОЕ МЕНЮ Основной Лист!!!
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_update, menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_update_close_without_save:
                finish();
                return true;
            case R.id.menu_update_save:
                updateData();
                return true;
            case R.id.menu_update_save_to_file:
                showMes("Save to file?", "OK?");
                return true;
            case R.id.menu_update_encrypt:
                // получение пароля из Преференс значений
                String pass = sp.getString("et_pass_prefs", "pass2Def");
                // проверка на совпадение с паролем по умолчанию
                if (pass == "pass2Def"){
                    showMesCryptCancel("Придумайте новый пароль", "измени пароль по умолчанию pass2Def на более сложный!!!");
                } else {
                    // проверка на длину не менее 8-ми символов (любых)
                    if (pass.length() < 8) {
                        showMesCryptCancel("Придумайте новый пароль", "Пароль слишком короткий, не менее 8-ми символов." +
                                "ВНИМАНИЕ!!! Придумайте такой пароль, который не забудете,ведь после\" +\n" +
                                "                                \"шифрования данных,в случае утери пароля, их восстановить будет не возможно!!! ");
                    } else {
                        showMesCrypt("Можно шифровать", "ВНИМАНИЕ!!! Ещё раз всё проверьте! Если Вы готовы, то можно приступать.");
                    }
                }
                item.setVisible(false);

                return true;
            case R.id.menu_update_decrypt:
               // showMesDecrypt("Расшифровка","Сейчас будет произведена расшифровка");
               //showMesPass();
                // вызов сообщения с паролем_______________________________________________________________________________
                String passPrefs = sp.getString("et_pass_prefs", "pass2Def");
                AlertDialogHelper dialogHelper = new AlertDialogHelper();
                dialogHelper.show(getSupportFragmentManager(), "TEST 1");
               // String passInput = dialogHelper.etInputPass.getText().toString();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    // создание папки
    public void mkDir(String path){
        File dir_path = new File(Environment.getExternalStorageDirectory(), path);
        if (!dir_path.exists()) {
            dir_path.mkdirs();
        } else {
            Toast.makeText(this,"Директория существует уже!!!", Toast.LENGTH_LONG).show();
        }
    }
    // КОПИРОВАНИЕ файла из Папки в Папку - РАБОТАЕТ!
    public void copyFilesToNewDir(File source, File destin) {
        mkDir("/MyNote/db/"); // создание папки перед копированием БД, если её ещё нет. Не знаю, работает или нет!
        try {
            FileChannel sourceChannel = null;
            FileChannel destinChannel = null;
            if (!destin.exists()) {   // проверка на отсутствие файла в пункте назначения
                destin.createNewFile();  // создание файла, если его ещё нет!
            }
            sourceChannel = new FileInputStream(source).getChannel();
            destinChannel = new FileOutputStream(destin).getChannel();
            destinChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
            // обязательно закрываем соединение
            sourceChannel.close();
            destinChannel.close();
        } catch (IOException ex) {
            Toast.makeText(getApplicationContext(),
                    "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
        }
    }
    // получение имени файла по первым 10-ти символам в тексте
    public String fileNameReturn(){
        int i = etUpdate.getText().length();
        char[] fileNameChar = new char[10];
        if (i <= 10) {  // РАБОТАЕТ! Заебись )
            etUpdate.getText().toString().getChars(0, i, fileNameChar, 0);
            return new String(fileNameChar);
        } else {
            etUpdate.getText().toString().getChars(0, fileNameChar.length, fileNameChar, 0);
        }
        return new String(fileNameChar);
    }
    // save data to file text_name.txt
    public void saveDataToFileTXT() {
        // получаем текст для имени сохраняемого файла из основного текста (15 знаков)
        char[] fileNameChar = new char[10];
        if (fileNameChar.length < 10) {  // нихрена не работает. Не знаю, пока, что делать!
        } else {
            try {
                File path = new File(Environment.getExternalStorageDirectory(),"/MyNote/files/"); //получаем путь,который сами указали в корне директории
                path.mkdirs(); // создаём папку - или проверка на существование папки, иначе будет ошибка
                File file = new File(path, fileNameReturn() + ".txt"); // присваиваем имя файлу
                // какие-то стандартные процедуры
                OutputStream os1 = new FileOutputStream(file);
                OutputStreamWriter osw = new OutputStreamWriter(os1);
                osw.write(etUpdate.getText().toString());   // инфа, которая запишется в файл из ЭдитТекста
                osw.close();  // обязательно закрываем соединение
                // обработка исключительной ошибки
            } catch (Throwable t) {
                Toast.makeText(getApplicationContext(),
                        "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
            }
        }
    }
        //сообщение , куда будет передаваться инфа для проверки
        // тестовое
        private void showMes(String title, String mes){
            AlertDialog.Builder ab = new AlertDialog.Builder(this);
            ab.create();
            ab.setCancelable(true);
            ab.setPositiveButton("Save?", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveDataToFileTXT();
                    //всплывающее сообщение Тост - сбоку справа
                }
            });
            ab.setTitle(title);
            ab.setMessage(mes);
            ab.show();
        }
    //сообщение , куда будет передаваться инфа для проверки
    // тестовое

    private void showMesCrypt(String title, String mes){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.create();
        ab.setCancelable(true);
        ab.setPositiveButton("Encrypt?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    CryptoHelper crypto = new CryptoHelper();
                    String str = etUpdate.getText().toString();
                    //crypto.encrypt(str);
                    etUpdate.setText(crypto.encrypt(str));
                } catch (Exception ex){
                    Log.e("Crypto", "AES encryption error");
                    Toast.makeText(getApplicationContext(),
                            "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
                }
                //всплывающее сообщение Тост - сбоку справа
                Toast.makeText(getApplicationContext(),
                        "Зашифровано " , Toast.LENGTH_LONG).show();
            }
        });
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }
    private void showMesDecrypt(String title, String mes){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.create();
        ab.setCancelable(true);

        ab.setPositiveButton("Encrypt?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    CryptoHelper decrypto = new CryptoHelper();
                    String str = etUpdate.getText().toString();
                    //crypto.encrypt(str);
                    etUpdate.setText(decrypto.decrypt(str));
                } catch (Exception ex){
                    Log.e("Decrypto", "AES decryption error");
                    Toast.makeText(getApplicationContext(),
                            "Exception: " + ex.toString(), Toast.LENGTH_LONG).show();
                }
                //всплывающее сообщение Тост - сбоку справа
                Toast.makeText(getApplicationContext(),
                        "Расшифровано " , Toast.LENGTH_LONG).show();
            }
        });
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }

    private void showMesCryptCancel(String title, String mes){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.create();
        ab.setCancelable(true);
        ab.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }

    private void showMesPass(){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("Title");
        ab.setMessage("Message");
        //LayoutInflater inflater = this.getLayoutInflater();
        LinearLayout view = (LinearLayout) getLayoutInflater().inflate(R.layout.dialog_enter_password, null);
       // ab.setView(inflater.inflate(R.layout.dialog_enter_password, null));
        //final EditText pass = new EditText(this);
        ab.setView(view);
        ab.setCancelable(true);
        ab.setPositiveButton("decrypt?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.create();
    }
        public void getListFiles(){
            File rootDir = Environment.getExternalStorageDirectory();
            File myDir = new File(rootDir, "/data/user/0/com.example.app3_5/databases/");
            for (File f : myDir.listFiles()) {
                if (f.isFile()) {
                    String str = f.getName();
                    etUpdate.setText( str);
                }
            }
        }
        public void DailogFragCall(View v){
        AlertDialogHelper dialogHelper = new AlertDialogHelper();
        dialogHelper.show(getSupportFragmentManager(), "TEST 1");
        }

        //the END
}

