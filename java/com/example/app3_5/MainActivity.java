package com.example.app3_5;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Fragment fragment = null;

    private TextView tvTrashCount;
    String countRow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        requestAppPermissions();

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.new_item);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
                startActivity(new Intent(MainActivity.this, INSERTActivity.class));
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvTrashCount = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_trash)); // ТВ для счётчика записей в Корзине

        //countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
        //showCountTrashItems(); //показ этого самого числа строк в Корзине
    }

    public static long backPress; // переменная для сохранения времени для двойного нажатия на кнопку НАЗАД и выхода
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // обработка двойного нажатия на кнопку НАЗАД для выхода
            if (backPress + 2000 > System.currentTimeMillis()) {
                super.onBackPressed();
            } else {
                //всплывающее сообщение Тост - сбоку справа
                Toast toastDel = Toast.makeText(getApplicationContext(), " ЕЩЁ раз для ВЫХОДА ", Toast.LENGTH_SHORT);
                toastDel.setGravity(Gravity.RIGHT, 150, 50);
                toastDel.show();
            }
            backPress = System.currentTimeMillis();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
        showCountTrashItems(); //показ этого самого числа строк в Корзине
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_list) {
            fragment = null;
            fragment = new FragLV();
            this.setTitle(R.string.title_activity_main);
            loadFragment(fragment);
        } else if (id == R.id.nav_search) {
            //fragment = new FragLV();
            //loadFragment(fragment);

        } else if (id == R.id.nav_trash) {
            fragment = new FragTRASH();
            loadFragment(fragment);
            this.setTitle(R.string.title_activity_trash);
        } else if (id == R.id.nav_optoins) {
            startActivity(new Intent(MainActivity.this, PREFActivity.class));

        } else if (id == R.id.nav_help) {
            fragment = new FragHELP();
            loadFragment(fragment);
            this.setTitle(R.string.title_activity_help);
        } else if (id == R.id.nav_author) {
            fragment = new FragABOUT();
            loadFragment(fragment);
            this.setTitle(R.string.title_activity_author);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void loadFragment(Fragment fragment){
        FragmentManager fragMan = getSupportFragmentManager();
       // FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        fragTrans.replace(R.id.fragment, fragment);
        fragTrans.commit();
    }

    //получение числа строк в таблице Корзина - для показа.
    public void countRowsFromTrashTable(){
        SQLiteHelper helper = new SQLiteHelper(getApplicationContext());
        Cursor cursor = helper.getCountRowsFromTrash();
        int i = cursor.getCount();
        countRow = String.valueOf(i);
    }
    // Показ числа записей из Корзины в Шторке
    public void showCountTrashItems(){
        tvTrashCount.setGravity(Gravity.CENTER_VERTICAL);
        tvTrashCount.setTypeface(null, Typeface.BOLD);
        tvTrashCount.setTextColor(getResources().getColor(R.color.colorAccent));
        tvTrashCount.setText(countRow);
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //PERMISSION CALLED
    private void requestAppPermissions() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        if (hasReadPermissions() && hasWritePermissions()) {
            return;
        }

        ActivityCompat.requestPermissions(this,
                new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1); // your request code
    }

    private boolean hasReadPermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private boolean hasWritePermissions() {
        return (ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

}
