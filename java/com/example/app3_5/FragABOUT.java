package com.example.app3_5;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragABOUT extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
                             Bundle savedInstance){
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        TextView tvAbout = (TextView) view.findViewById(R.id.tv_about_fragment);
        tvAbout.setText("Моё имя Михаил." + " \n\n "+
                "Я хочу стать программистом" + "\n\n" +
                "А это моё первое приложение." + "\n\n" +
                "Самый обычный простой блокнот." + "\n\n" +
                "Версия приложения V-0.3.7" + "\n\n" +
                "Почтовый ящик для связи со мной:" + "\n\n" +
                "ste1n8860@gmail.com" + "\n\n");
        return view;
    }
}
