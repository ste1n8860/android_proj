package com.example.app3_5;


import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class FragLV extends Fragment {

    ListView lv;

    SQLiteHelper helper;
    SQLiteDatabase db1;
    Cursor cursor;
    lv_adapter_2 adapter2;

    String stringID1 = null;
    int intID = 0;

    // массивы для адаптера
    ArrayList<String> ID_array = new ArrayList<String>();
    ArrayList<String> TEXT_array = new ArrayList<String>();
    ArrayList<String> DATE_array = new ArrayList<String>();

    public FragLV() {
        // Required empty public constructor
    }
/*
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        } */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lv, container, false);

        lv = (ListView) view.findViewById(R.id.lv_items);
        registerForContextMenu(lv);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               //startActivity(new Intent(getActivity(), UPDATEActivity.class));
                getIdLabelFromList(position);
                // попытка отправки данных на редактирование в
                // другую Активити
                Intent intent = new Intent(getActivity(), UPDATEActivity.class);
                intent.putExtra("ids", stringID1);
                startActivity(intent);
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                helper = new SQLiteHelper(getActivity());
                helper.getIdLabel();
                // создаём список , в который заносим элементы Массива с ИД-ами из базы
                List listokID = helper.getIdLabel();
                // заносим значение выбранного ИД-а в переменную строковую
                stringID1 = listokID.get(position).toString();
                Toast.makeText(getActivity(),"Выбрано - " + stringID1, Toast.LENGTH_LONG).show();
                intID = listokID.indexOf(position);
           // getIdLabelFromList(position);
                return false;
            }
        });

        helper = new SQLiteHelper(getActivity());
        showDataSQL();
        return view;
    }

    @Override
    public void onResume() {
        showDataSQL();
        // loadListViewData();
       // countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
        //showCountTrashItems(); //показ этого самого числа строк в Корзине
        super.onResume();
    }

    // КОНТЕКСТНОЕ МЕНЮ Основной Лист!!!
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_lv, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update_lv_menu:
                 updateDataInRow();
                return true;
            case R.id.delete_lv_menu:
                 replaceData();
                return true;
            case R.id.clone_lv_menu:
                cloneRow();
                return true;
            case R.id.search_lv_menu:
                //toast("baz");
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    // МОЙ КОД------------------------------------------------------------------------------
    // перемещение к ворзину удалённой строки
    public void replaceData() {
        helper = new SQLiteHelper(getActivity());
        db1 = helper.getWritableDatabase();
        cursor = helper.getRowAll(stringID1);
        String idTrash;
        String textTrash;
        String dateTrash;
        if (cursor.moveToFirst()) {
            do {
                idTrash = cursor.getString(cursor.getColumnIndex("_id"));
                textTrash = cursor.getString(cursor.getColumnIndex("text"));
                dateTrash = cursor.getString(cursor.getColumnIndex("date"));
            } while (cursor.moveToNext());

            helper.replaceWrightRow( textTrash, dateTrash);

            //Toast.makeText(getActivity(), "удалено УСПЕШНО!!!", Toast.LENGTH_LONG).show();
            showMes("перемещено в корзину!!!", idTrash);
            helper.deleteRow(stringID1);
            showDataSQL();
            sendToActivity(); // отправка числа удалённых записей в Активити
            //countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
            //showCountTrashItems(); //показ этого самого числа строк в Корзине
        }
        db1.close();
        cursor.close();
    }
    public void updateDataInRow() {
        helper = new SQLiteHelper(getActivity());
        // попытка отправки данных на редактирование в
        // другую Активити
        Intent intent = new Intent(getActivity(), UPDATEActivity.class);
        intent.putExtra("ids", stringID1);
        startActivity(intent);
    }

    //сообщение , куда будет передаваться инфа для проверки
    // тестовое
    private void showMes(String title, String mes) {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.create();
        ab.setCancelable(true);
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }

    // КЛОНИРОВАНИЕ
    public void cloneRow(){
        helper = new SQLiteHelper(getActivity());
        db1 = helper.getWritableDatabase();
        cursor = helper.getRowAll(stringID1);
        String idClone;
        String textClone;
        String dateClone;
        if (cursor.moveToFirst()){
            do {
                idClone = cursor.getString(cursor.getColumnIndex("_id"));
                textClone = cursor.getString(cursor.getColumnIndex("text"));
                dateClone = cursor.getString(cursor.getColumnIndex("date"));
            } while (cursor.moveToNext());
            helper.insertData(textClone, dateClone);
            showMes("Клонирована запись - !!!", idClone);
            showDataSQL();
        }
    }

    // получаем Ид строки из базы через Ид листа за одну обработку!!! _ РАБОТАЕТ
    public String getIdLabelFromList(int positionFromList){
        helper = new SQLiteHelper(getActivity());
        helper.getIdLabel();
        // создаём список , в который заносим элементы Массива с ИД-ами из базы
        List listokID = helper.getIdLabel();
        // заносим значение выбранного ИД-а в переменную строковую
        stringID1 = listokID.get(positionFromList).toString();
        intID = listokID.indexOf(positionFromList);
        return stringID1;
    }
    //получение числа строк в таблице Корзина - для показа.
    public String countRowsFromTrashTable(){
        SQLiteHelper helper = new SQLiteHelper(getActivity());
        Cursor cursor = helper.getCountRowsFromTrash();
        int i = cursor.getCount();
        //String countRow = String.valueOf(i);
        return String.valueOf(i);
    }

    // ОТПРАВКА ЧИСЛА ЗАПИСЕЙ В КОРЗИНЕ В АКТИВИТИ - РАБОТАЕТ. САМ.
    public void sendToActivity(){
        TextView tv1 = (TextView) getActivity().findViewById(R.id.nav_trash);
        String str = countRowsFromTrashTable();
        tv1.setText(str);
    }

    // показать данные из базы в Лист
    private void showDataSQL() {
        db1 = helper.getWritableDatabase();
        cursor = db1.rawQuery(" SELECT * FROM listok4 ", null);
        ID_array.clear();
        TEXT_array.clear();
        DATE_array.clear();
        if (cursor.moveToFirst()) {
            do {
                ID_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_ID)));
                TEXT_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_NAME)));
                DATE_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.COLUMN_DATE)));
            } while (cursor.moveToNext());
        }
        adapter2 = new lv_adapter_2(getActivity(),
                ID_array,
                TEXT_array,
                DATE_array);
        lv.setAdapter(adapter2);
        db1.close();
        cursor.close();
    }

}
