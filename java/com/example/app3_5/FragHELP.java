package com.example.app3_5;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragHELP extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstance) {
        View view = inflater.inflate(R.layout.fragment_help, container, false);
        TextView tvHelp = (TextView) view.findViewById(R.id.tv_help_fragment);
        tvHelp.setText("Краткая справка по работе с приложением. " + "\n\n" +
                "В моём Блокноте реализованы ещё не все функции," +
                "которые я хотел бы сделать, но обязательно сделаю. " +
                "Основной функционал работает исправно. " +
                "Все удалённые записи перемещаются в корзину, " +
                "а уже оттуда удаляются совсем. " +
                "Так как приложение ещё совсем *сырое*, то " +
                "иногда могут возникать ошибки. " + "\n\n" +
                "Я их исправлю со временем. Всем спасибо! " + "\n\n" +
                "Почтовый ящик для связи со мной: ste1n8860@gmail.com" + "\n\n" +
                "  " + "\n\n" +
                "  Изменения в этой версии:" + "\n" +
                "- исправил мелкие баги и недочёты"  +"\n" +
                "- исправил отображене текста в Листе" +"\n" +
                "- добавил вкладку настройки" +"\n" +
                "- ДОСТУПНО изменение размера шрифта в окнах ввода" + "\n" +
                "- локализация приложения ENG" + "\n" +
                "- поддержка экранов разных размеров" + "\n" +
                "- контекстное меню (долгое нажатие)" + "\n" +
                "- версия приложения" + "\n" +
                "- изменение иконок и интерфейса" + "\n" +
                "- добавил клонирование записи" + "\n" +
                "- добавил восстановление из корзины" + "\n" +
                "- убрал кнопки из интерфейса лишние" + "\n" +
                "- кол-во удалённых записей в Корзине" + "\n" +
                "- мелкие изменение в интерфейсе" + "\n" +
                "- отображение текста в одну строку с Списке" + "\n" +
                "- изменил цветовую гамму и стиль" +"\n" +
                "- убрал лишние Тосты");
        return view;
    }
}
