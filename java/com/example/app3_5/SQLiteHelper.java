package com.example.app3_5;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class SQLiteHelper extends SQLiteOpenHelper {
    // public static final int DATABASE_VERSION = 1;
    public static String DATA_BASE_PATH = "/storage/emulated/0/MyNote/db/";
    public static final String DATABASE_NAME = "spisok4";
    public static final String TABLE_NAME = "listok4";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "text";
    public static final String COLUMN_DATE = "date";

    public static final String TABLE_TRASH = "trash4";
    public static final String TRASH_ID = "_id";
    public static final String TRASH_TEXT = "text";
    public static final String TRASH_DATE = "date";

    public SQLiteHelper(Context context) {
        super(context, DATA_BASE_PATH+DATABASE_NAME, null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_TABLE = " CREATE TABLE " + TABLE_NAME + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + COLUMN_NAME + " TEXT , " + COLUMN_DATE + " NUMERIC ) ";
        database.execSQL(CREATE_TABLE);
        String CREATE_TABLE_TRASH = " CREATE TABLE " + TABLE_TRASH + " ( " + TRASH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " + TRASH_TEXT + " TEXT , " + TRASH_DATE + " NUMERIC ) ";
        database.execSQL(CREATE_TABLE_TRASH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVers, int newVers) {
        database.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
        database.execSQL(" DROP TABLE IF EXISTS " + TABLE_TRASH);
        onCreate(database);
    }

    public boolean insertData(String text, String date) {
        SQLiteDatabase db12 = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, text);
        contentValues.put(COLUMN_DATE, date);
        long result = db12.insert(TABLE_NAME, null, contentValues);
        //check if value is -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean insertDataToTrash(String text, String date) {
        SQLiteDatabase db12 = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRASH_TEXT, text);
        contentValues.put(TRASH_DATE, date);
        long result = db12.insert(TABLE_TRASH, null, contentValues);
        // check if value is -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }


    // моя выгрузка всех данных для трёх данных в одну строку
    public List getFullLabels() {
        List list = new ArrayList();
        //select all query
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        // открываем подключение
        SQLiteDatabase db = this.getReadableDatabase();
        //получаем данные из бд в виде курсора
        Cursor cur = db.rawQuery(selectQuery, null); //select query, selected arguments
        // цикл по всем записям и добавление в Лист
        if (cur.moveToFirst()) {
            do {
                // отображение значений всех ячеек строки из базы в строке Листа
                list.add(cur.getString(0) + " " + cur.getString(1));
            } while (cur.moveToNext());
        }
        cur.close();
        db.close();
        //вернём значения строк в Лист
        return list;
    }

    // получение ИДа для основной таблицы
    public ArrayList getIdLabel() {
        // создаём массив для занесения туда ИД-ов строк из базы
        ArrayList listId = new ArrayList<>();
        //стандартно выбираем все элементы из базы, хотя можно только столбец с ИД-ами выбрать. Да пофиг!
        // только по ИД
        String selectQuery = "SELECT _id FROM " + TABLE_NAME;
        // открываем подключение к базе
        SQLiteDatabase db = this.getReadableDatabase();
        //получаем данные из бд в виде курсора
        Cursor cur = db.rawQuery(selectQuery, null); //select query, selected arguments
        // цикл по всем записям и добавление в Лист
        if (cur.moveToFirst()) {
            do {
                listId.add(cur.getString(0));
            } while (cur.moveToNext());
        }
        cur.close();
        db.close();
        //вернём значения строк в Лист
        return listId;
    }

    // получение ИДа для основной таблицы
    public ArrayList getIdLabelFromTrash() {
        // создаём массив для занесения туда ИД-ов строк из базы
        ArrayList listId = new ArrayList<>();
        //стандартно выбираем все элементы из базы, хотя можно только столбец с ИД-ами выбрать. Да пофиг!
        // только по ИД
        String selectQuery = "SELECT _id FROM " + TABLE_TRASH;
        // открываем подключение к базе
        SQLiteDatabase db = this.getReadableDatabase();
        //получаем данные из бд в виде курсора
        Cursor cur = db.rawQuery(selectQuery, null); //select query, selected arguments
        // цикл по всем записям и добавление в Лист
        if (cur.moveToFirst()) {
            do {
                listId.add(cur.getString(0));
            } while (cur.moveToNext());
        }
        cur.close();
        db.close();
        //вернём значения строк в Лист
        return listId;
    }

    // попытка удаления строки из базы --- РАБОТАЕТ
    public void deleteRow(String id) {
        getWritableDatabase().delete(TABLE_NAME, COLUMN_ID + " =? ", new String[]{id});
    }
    // попытка удаления строки из базы --- РАБОТАЕТ
    public void deleteRowFromTrash(String id) {
        getWritableDatabase().delete(TABLE_TRASH, TRASH_ID + " =? ", new String[]{id});
    }

    //попытка редактирования выбраной записи в таблице
    public boolean updateRow(String id, String text, String date) {
        SQLiteDatabase db8 = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, id);
        cv.put(COLUMN_NAME, text);
        cv.put(COLUMN_DATE, date);
        int i = db8.update(TABLE_NAME, cv, "_id = ?", new String[]{id});
        return i > 0;
    }

    //попытка записи перенесённой строки из одной таблицы в другую
    public boolean replaceWrightRow( String text, String date) {
        SQLiteDatabase db8 = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        //cv.put(TRASH_ID, id);
        cv.put(TRASH_TEXT, text);
        cv.put(TRASH_DATE, date);
        long i = db8.insert(TABLE_TRASH,  null, cv);
        return i > 0;
    }
    //попытка записи перенесённой строки из одной таблицы в другую
    public boolean restoreFromTrashToList( String text, String date) {
        SQLiteDatabase db8 = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        //cv.put(TRASH_ID, id);
        cv.put(COLUMN_NAME, text);
        cv.put(COLUMN_DATE, date);
        long i = db8.insert(TABLE_NAME,  null, cv);
        return i > 0;
    }

    //
    public Cursor getRow(String id) {
        SQLiteDatabase db7 = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + "listok4" + "WHERE _id='" + id + "'";
        Cursor cursor = db7.rawQuery(selectQuery, null);
        return cursor;
    }

    // попытка получения количества строк в Корзине
    public Cursor getCountRowsFromTrash() {
        SQLiteDatabase db7 = this.getWritableDatabase();
        String selectQuery = " SELECT * FROM " + "trash4" ;
        Cursor cursor = db7.rawQuery(selectQuery, null);
        return cursor;
    }

    // рабочая версия получения ВСЕЙ СТРОКИ из таблицы
    // для переноса одной строки в другую
    public Cursor getRowAll(String id) {
        SQLiteDatabase db9 = this.getWritableDatabase();
        //String selectQuery = " SELECT * FROM " + TABLE_NAME + "WHERE _id = ' " +id+" '";
        Cursor cursor = db9.rawQuery("SELECT * FROM  listok4  WHERE _id = ?", new String[]{id});
        return cursor;
    }
    // рабочая версия получения ВСЕЙ СТРОКИ из таблицы
    // для переноса одной строки в другую
    public Cursor getRowAllFromTrash(String id) {
        SQLiteDatabase db9 = this.getWritableDatabase();
        Cursor cursor = db9.rawQuery("SELECT * FROM  trash4  WHERE _id = ?", new String[]{id});
        return cursor;
    }

    // рабочая версия получения части --ТЕКСТ-- из строки таблицы
    public Cursor getRow2(String id) {
        SQLiteDatabase db9 = this.getWritableDatabase();
        //String selectQuery = " SELECT * FROM " + TABLE_NAME + "WHERE _id = ' " +id+" '";
        Cursor cursor = db9.rawQuery("SELECT text FROM  listok4  WHERE _id = ?", new String[]{id});
        return cursor;
    }

    public void clearTableTrash(){
        SQLiteDatabase db10 = this.getWritableDatabase();
        db10.delete(TABLE_TRASH, null, null);
    }
}