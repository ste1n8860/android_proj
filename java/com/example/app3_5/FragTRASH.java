package com.example.app3_5;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragTRASH extends Fragment {

    ListView lvTrash;

    SQLiteHelper helper;
    SQLiteDatabase db1;
    Cursor cursor;
    lv_adapter_2 adapter2;

    String stringID = null;
    int intID = 0;

    // массивы для адаптера
    ArrayList<String> ID_array = new ArrayList<String>();
    ArrayList<String> TEXT_array = new ArrayList<String>();
    ArrayList<String> DATE_array = new ArrayList<String>();

    public FragTRASH() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trash, container, false);

      // Snackbar.make(container, "КОрзина очищена!", Snackbar.LENGTH_SHORT).show();

        lvTrash = (ListView) view.findViewById(R.id.lv_trash);
        registerForContextMenu(lvTrash);
        lvTrash.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                //stringID = lv.getItemAtPosition(position).toString();
                helper = new SQLiteHelper(getActivity());
                helper.getIdLabel();
                // создаём список , в который заносим элементы Массива с ИД-ами из базы
                List listokID = helper.getIdLabelFromTrash();
                // заносим значение выбранного ИД-а в переменную строковую
                stringID = listokID.get(position).toString();
                intID = listokID.indexOf(position);
                return false;
            }
        });

        helper = new SQLiteHelper(getActivity());
        showDataSQLinTrash();
        return view;
    }

    @Override
    public void onResume() {
        showDataSQLinTrash();
        // countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
        //showCountTrashItems(); //показ этого самого числа строк в Корзине
        super.onResume();
    }

    // показать данные из базы в Лист
    private void showDataSQLinTrash() {
        db1 = helper.getWritableDatabase();
        cursor = db1.rawQuery(" SELECT * FROM trash4 ", null);
        ID_array.clear();
        TEXT_array.clear();
        DATE_array.clear();
        if (cursor.moveToFirst()) {
            do {
                ID_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.TRASH_ID)));
                TEXT_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.TRASH_TEXT)));
                DATE_array.add(cursor.getString(cursor.getColumnIndex(SQLiteHelper.TRASH_DATE)));
            } while (cursor.moveToNext());
        }
        adapter2 = new lv_adapter_2(getActivity(),
                ID_array,
                TEXT_array,
                DATE_array);
        lvTrash.setAdapter(adapter2);
        db1.close();
        cursor.close();
    }

    //сообщение , куда будет передаваться инфа для проверки
    // тестовое
    private void showMes(String title, String mes){
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.create();
        ab.setCancelable(true);
        ab.setPositiveButton("ОЧИСТИТЬ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                helper = new SQLiteHelper(getActivity());
                helper.clearTableTrash();
                //всплывающее сообщение Тост - сбоку справа
                Toast toastDel = Toast.makeText(getActivity()," Успешно ОЧИЩЕНА!!! ", Toast.LENGTH_SHORT);
                toastDel.setGravity(Gravity.RIGHT,150,100);
                toastDel.show();
                showDataSQLinTrash();
                sendToActivity();
            }
        });
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }

    //сообщение , куда будет передаваться инфа для проверки
    // тестовое
    private void showMes1(String title, String mes) {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.create();
        ab.setCancelable(true);
        ab.setTitle(title);
        ab.setMessage(mes);
        ab.show();
    }

    // КОНТЕКСТНОЕ МЕНЮ Основной Лист!!!
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_trash, menu);
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update_menu_trash:
                // updateDataInRow();
                return true;
            case R.id.empty_menu_trash:
                showMes("Очистка корзины!!!", " Вы действительно хотите полностью очистить корзину???");
                return true;
            case R.id.return_menu_trash:
                restoreRowFromTrash();
                return true;
            case R.id.search_menu_trash:
                //toast("baz");
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void restoreRowFromTrash(){
        helper = new SQLiteHelper(getActivity());
        db1 = helper.getWritableDatabase();
        cursor = helper.getRowAllFromTrash(stringID);
        String idTrash;
        String textTrash;
        String dateTrash;
        if (cursor.moveToFirst()) {
            do {
                idTrash = cursor.getString(cursor.getColumnIndex("_id"));
                textTrash = cursor.getString(cursor.getColumnIndex("text"));
                dateTrash = cursor.getString(cursor.getColumnIndex("date"));
            } while (cursor.moveToNext());

            //helper.restoreFromTrashToList( textTrash, dateTrash);
            helper.insertData(textTrash, dateTrash);

            //Toast.makeText(getActivity(), "удалено УСПЕШНО!!!", Toast.LENGTH_LONG).show();
            showMes1("Успешно восстановлено!!!", idTrash);

            helper.deleteRowFromTrash(stringID);
            showDataSQLinTrash();
            sendToActivity();
            //countRowsFromTrashTable(); // обработка для показа из Курсора числа строк и перевод в строку
            //showCountTrashItems(); //показ этого самого числа строк в Корзине
        }
        db1.close();
        cursor.close();
    }

    //получение числа строк в таблице Корзина - для показа.
    public String countRowsFromTrashTable(){
        SQLiteHelper helper = new SQLiteHelper(getActivity());
        Cursor cursor = helper.getCountRowsFromTrash();
        int i = cursor.getCount();
        //String countRow = String.valueOf(i);
        return String.valueOf(i);
    }

    // ОТПРАВКА ЧИСЛА ЗАПИСЕЙ В КОРЗИНЕ В АКТИВИТИ - РАБОТАЕТ. САМ.
    public void sendToActivity(){
        TextView tv1 = (TextView) getActivity().findViewById(R.id.nav_trash);
        String str = countRowsFromTrashTable();
        tv1.setText(str);
    }

}
