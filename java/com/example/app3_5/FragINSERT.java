package com.example.app3_5;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragINSERT extends Fragment {

    SQLiteHelper helper;
    SQLiteDatabase db1;

    Button btnInsert, btnAllLisr;
    EditText etInsert;

    Boolean checkEmptyEDIT;

    String dateTime, text;

    public FragINSERT() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_insert, container, false);
        helper = new SQLiteHelper(getActivity());

        btnInsert = (Button) view.findViewById(R.id.btn1_insert_fragment);
        etInsert = (EditText) view.findViewById(R.id.et_insert_text_fragment);

        insertDataToSQL();

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }

    public void insertDataToSQL() {
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //DBCreate();
            insertData();

               // getDateTime();
               // helper.insertData(etInsert.getText().toString(), dateTime);
               // Toast.makeText(getActivity(),"Информация внесена УСПЕШНО!!!", Toast.LENGTH_LONG).show();
               // clearETafter();
            }
        });
    }

    public void getDateTime(){
        // Текущие дата и время
        //db1 = helper.onCreate();
        Date currentDate = new Date();
        // Форматирование времени как "день.месяц.год"
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String dateText = dateFormat.format(currentDate);
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss" , Locale.getDefault());
        String timeText = timeFormat.format(currentDate);
        String date_time = dateText+ "  " +timeText;

        dateTime = date_time;
    }
    public void insertData(){

        getDateTime();
        text = etInsert.getText().toString();
        //
        checkETemptyOrNot(text, dateTime);

        if (checkEmptyEDIT == true){
           // sqliteQuery = " INSERT INTO listok4 (text, date) VALUES ('" + text + "', '" + dateTime + "' )";
           // db1.execSQL(sqliteQuery);
            helper.insertData(text, dateTime);

            //спрячем клавиатуру
           //InputMethodManager inputMM = (InputMethodManager) getActivity(getActivity().INPUT_METHOD_SERVICE);
           // inputMM.hideSoftInputFromWindow(etInsert.getWindowToken(), 0);

            Toast.makeText(getActivity(),"Информация внесена УСПЕШНО!!!", Toast.LENGTH_LONG).show();
            clearETafter();
        }else{
            Toast.makeText(getActivity(),"Заполни ВСЕ поля", Toast.LENGTH_LONG).show();
        }
    }


    public void DBCreate(){
        db1 = SQLiteDatabase.openOrCreateDatabase("spisok4", null, null); //вообще ХЗ, что это такое
        db1.execSQL(" CREATE TABLE IF NOT EXISTS listok4 ( _id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , text TEXT , date NUMERIC );");
    }

    // проверка на заполнение поля с текстом
    public void checkETemptyOrNot(String name, String dateTime){
        // if(TextUtils.isEmpty(name) || TextUtils.isEmpty(dateTime) || TextUtils.isEmpty(subject)){
        if(TextUtils.isEmpty(name)){
            checkEmptyEDIT = false;
        }else {
            checkEmptyEDIT = true;
        }
    }

    // очистка текстовых полей
    public  void clearETafter(){
        etInsert.getText().clear();
        // getDate.getText().clear();
        //getSubject.getText().clear();
    }

}
