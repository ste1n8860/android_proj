package com.example.app3_5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class lv_adapter_2 extends BaseAdapter {

    Context context;
    ArrayList<String> userID;
    ArrayList<String> userTEXT;
    ArrayList<String> userDATE;

    public lv_adapter_2(Context context1,
                        ArrayList<String> id,
                        ArrayList<String> text,
                        ArrayList<String> date){
        this.context = context1;
        this.userID = id;
        this.userTEXT = text;
        this.userDATE = date;
    }

    @Override
    public int getCount(){
        return userID.size();
    }
    @Override
    public Object getItem(int position){
        return position;
    }
    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public boolean isEnabled(int position){
        return true;
    }

    public View getView(int position, View child, ViewGroup parent){
        Holder2 holder;
        LayoutInflater inflater;

        if (child == null){
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = inflater.inflate(R.layout.lv_data_new_view, parent,    false);

            holder = new Holder2();
            //holder.textViewID = (TextView) child.findViewById(R.id.tvID_new);
            holder.textViewTEXT = (TextView) child.findViewById(R.id.tvNAME_new);
            holder.textViewDATE = (TextView) child.findViewById(R.id.tvDATETIME_new);

            child.setTag(holder);
        }else {
            holder = (Holder2) child.getTag();
        }
       // holder.textViewID.setText(userID.get(position));
        holder.textViewTEXT.setText(userTEXT.get(position));
        holder.textViewDATE.setText(userDATE.get(position));
        return child;

    }

    public class Holder2{
       // TextView textViewID;
        TextView textViewTEXT;
        TextView textViewDATE;
    }


}
